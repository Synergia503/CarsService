import { NgModule } from "@angular/core";
import { RouterModule, Route } from '@angular/router';
import { AuthCanLoadGuard } from "./guards/auth-can-load.guard";
import { PageNotFoundComponent } from './shared-module/page-not-found/page-not-found.component';
import { LoginComponent } from "./login/login.component";

const routes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'login' },
  { path: 'login', component: LoginComponent },
  { path: 'cars', canLoad: [AuthCanLoadGuard], loadChildren: 'app/cars/cars.module#CarsModule' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: true })],
  exports: [RouterModule]
})

export class AppRoutingModule { }
