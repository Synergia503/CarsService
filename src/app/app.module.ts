import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core-module/core.module';
import { CarsService } from "./cars/index";
import { LoginModule } from './login/login.module';
import { AuthService } from './auth/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { LayoutService } from './shared-module/services/layout.service';
import { SharedModule } from './shared-module/shared.module';
import { AuthCanLoadGuard } from './guards/auth-can-load.guard';
import { FormCanDeactivateGuard } from './guards/form-can-deactivate.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LoginModule,
    CoreModule],
  providers: [CarsService, AuthService, AuthCanLoadGuard, AuthGuard, FormCanDeactivateGuard, LayoutService],
  bootstrap: [AppComponent]
})
export class AppModule { }
