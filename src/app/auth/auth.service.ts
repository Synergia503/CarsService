import { Injectable } from '@angular/core';
import { reject, resolve } from 'q';
import { LayoutService } from '../shared-module/services/layout.service';

@Injectable()
export class AuthService {

  //fake credentials
  private credentials = {
    login: 'admin',
    password: 'admin'
  };

  private isUserLoggedIn: boolean = false;

  constructor(private layoutService: LayoutService) { }

  // fake login
  login(login: string, password: string) {
    return new Promise((resolve, reject) => {
      if (login === this.credentials.login && password === this.credentials.password) {
        this.isUserLoggedIn = true;
        resolve();
      }
      else {
        reject();
      }
    });
  }

  logout() {
    this.isUserLoggedIn = false;
    this.layoutService.hideSidebar();
  }

  isLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }
}
