import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, ComponentRef } from '@angular/core';
import { CarsService } from '../cars.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from '../models/car';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { DateInfoComponent } from './date-info/date-info.component';

@Component({
  selector: 'cs-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.less']
})
export class CarDetailsComponent implements OnInit {

  @ViewChild('dateInfoContainer', { read: ViewContainerRef }) dateInfoContainerRef: ViewContainerRef;
  car: Car;
  carForm: FormGroup;
  elapsedDays: number;
  dateInfoRef: ComponentRef<DateInfoComponent>;

  constructor(
    private carsService: CarsService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit() {
    this.loadCar();
    this.carForm = this.buildCarForm();
  }

  createDateInfoComponent() {
    // console.log(this.componentFactoryResolver);
    if (this.dateInfoContainerRef.get(0) !== null) {
      return;
    }

    const dateInfoFactory = this.componentFactoryResolver.resolveComponentFactory(DateInfoComponent);
    this.dateInfoRef = this.dateInfoContainerRef.createComponent(dateInfoFactory);

    this.dateInfoRef.instance.car = this.car;
    this.dateInfoRef.instance.checkElapsedDays.subscribe(val => {
      this.elapsedDays = val;
    });
  }

  clearDateInfoContainer() {
    // this.dateInfoContainerRef.clear();
    // this.dateInfoContainerRef.remove(0);
    this.dateInfoRef.destroy();
  }

  loadCar() {
    this.car = this.route.snapshot.data['car'];
  }

  buildCarForm() {
    let parts = this.car.parts.map(p => this.formBuilder.group(p));
    return this.formBuilder.group({
      model: [this.car.model, Validators.required],
      type: this.car.type,
      plate: [this.car.plate, [Validators.required, Validators.minLength(3), Validators.maxLength(8)]],
      deliveryDate: this.car.deliveryDate,
      deadline: this.car.deadline,
      color: this.car.color,
      power: this.car.power,
      clientFirstName: this.car.clientFirstName,
      clientSurname: this.car.clientSurname,
      year: this.car.year,
      isFullyDamaged: this.car.isFullyDamaged,
      parts: this.formBuilder.array(parts)
    });
  }

  updateCar() {
    let carFormData = Object.assign({}, this.carForm.value);
    carFormData.cost = this.getPartsCost(carFormData.parts);
    this.carsService.updateCar(this.car.id, carFormData)
      .subscribe(() => {
        this.router.navigate(['/cars']);
      });
  }

  getPartsCost(parts) {
    return parts.reduce((prev, nextPart) => {
      return parseFloat(prev) + parseFloat(nextPart.price);
    }, 0);
  }

  buildParts(): FormGroup {
    return this.formBuilder.group({
      name: '',
      inStock: true,
      price: ''
    });
  }

  get parts(): FormArray {
    return <FormArray>this.carForm.get('parts');
  }

  addPart(): void {
    this.parts.push(this.buildParts());
  }

  removePart(i: number): void {
    this.parts.removeAt(i);
  }

}
