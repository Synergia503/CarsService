import { Component, Input, Output, EventEmitter, HostBinding, OnInit, HostListener, Renderer2, ElementRef } from '@angular/core';
import { Car } from '../models/car';

@Component({
  selector: '[cs-car-table-row]',
  templateUrl: './car-table-row.component.html'
})
export class CarTableRowComponent implements OnInit {

  @Input() car: Car;
  @Output() removedCar = new EventEmitter();
  @HostBinding('class.after-deadline') deadline: boolean = false;

  @HostListener('mouseenter') onMouseEnter() {
    this.setRemoveButtonStyle('red');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setRemoveButtonStyle('black');
  }

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    this.deadline = new Date(this.car.deadline) < new Date();
  }

  removeCar(car: Car, event) {
    event.stopPropagation();
    this.removedCar.emit(car);
  }

  private setRemoveButtonStyle(color: string) {
    this.renderer.setStyle(this.el.nativeElement.querySelector('.remove-btn'), 'color', color);
  }
}
