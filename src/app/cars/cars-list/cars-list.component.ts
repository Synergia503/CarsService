import { Component, OnInit, ViewEncapsulation, ViewChild, AfterViewInit, ViewChildren, QueryList, ElementRef, Renderer2, AfterViewChecked } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Car } from '../models/car';
import { TotalCostComponent } from '../total-cost/total-cost.component';
import { CarsService } from '../cars.service';
import { CostSharedService } from '../cost-shared.service';
import { CarTableRowComponent } from '../car-table-row/car-table-row.component';
import { CsValidators } from '../../shared-module/validators/cs-validators';
import { CanComponentDeactivate } from '../../guards/form-can-deactivate.guard';

@Component({
  selector: 'cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class CarsListComponent implements OnInit, AfterViewInit, AfterViewChecked, CanComponentDeactivate {


  @ViewChild("addCarTitle") addCarTitle: ElementRef;
  @ViewChild("totalCostRef") totalCostRef: TotalCostComponent;
  @ViewChildren(CarTableRowComponent) carRows: QueryList<CarTableRowComponent>;

  grossCost: number;
  totalCost: number;
  cars: Car[];
  carForm: FormGroup;
  counter = 0;

  constructor(
    private carsService: CarsService,
    private formBuilder: FormBuilder,
    private router: Router,
    private costSharedService: CostSharedService,
    private renderer: Renderer2) { }

  // hook of component's lifecycle
  // initialized on component's start
  ngOnInit() {
    this.loadCars();
    this.carForm = this.buildCarForm();
    //no access to i.e. methods from nested components' methods with @ViewChild reference
  }

  // initialized when view was build, there is an access to nested components here
  ngAfterViewInit() {
    const addCarTitle = this.addCarTitle.nativeElement;

    this.carForm.valueChanges.subscribe(() => {
      if (this.carForm.invalid) {
        // addCarTitle.style.color = 'red';
        this.renderer.setStyle(addCarTitle, 'color', 'red');
      }
      else {
        // addCarTitle.style.color = 'white';
        this.renderer.setStyle(addCarTitle, 'color', 'white');
      }
    });

    this.carRows.changes.subscribe(() => {
      if (this.carRows.first.car.clientSurname === 'Kowalski') {
        console.log('Warning, Client Kowalski is next in queue.');
      }
    });
  }

  ngAfterViewChecked() {
    this.counter++;
    console.log(this.counter);
  }

  buildCarForm() {
    return this.formBuilder.group({
      model: ['', Validators.required],
      type: '',
      plate: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(8)]],
      deliveryDate: '',
      deadline: '',
      color: '',
      power: ['', CsValidators.power],
      clientFirstName: '',
      clientSurname: '',
      year: '',
      isFullyDamaged: '',
      parts: this.formBuilder.array([])
    });
  }

  buildParts(): FormGroup {
    return this.formBuilder.group({
      name: '',
      inStock: true,
      price: ''
    });
  }

  get parts(): FormArray {
    return <FormArray>this.carForm.get('parts');
  }

  addPart(): void {
    this.parts.push(this.buildParts());
  }

  removePart(i: number): void {
    this.parts.removeAt(i);
  }

  togglePlateValidity() {
    const damageControl = this.carForm.get('isFullyDamaged');
    const plateControl = this.carForm.get('plate');

    if (damageControl.value) {
      plateControl.clearValidators();
    }
    else {
      plateControl.setValidators([Validators.required, Validators.minLength(3), Validators.maxLength(8)]);
    }

    plateControl.updateValueAndValidity();
  }

  loadCars(): void {
    this.carsService.getCars()
      .subscribe(cars => {
        this.cars = cars;
        this.countTotalCost();
        this.costSharedService.shareCost(this.totalCost);
      });
  }

  addCar() {
    let carFormData = Object.assign({}, this.carForm.value);
    carFormData.cost = this.getPartsCost(carFormData.parts);
    this.carsService.addCar(carFormData)
      .subscribe(() => {
        this.loadCars();
      });
  }

  getPartsCost(parts) {
    return parts.reduce((prev, nextPart) => {
      return parseFloat(prev) + parseFloat(nextPart.price);
    }, 0);
  }

  onRemovedCar(car: Car) {
    this.carsService.removeCar(car.id)
      .subscribe(() => {
        this.loadCars();
      });
  }

  countTotalCost(): void {
    this.totalCost = this.cars
      .map((car) => car.cost)
      .reduce((prev, next) => { return prev + next }, 0);
  }

  goToCarDetails(car: Car) {
    this.router.navigate(['/cars', car.id]);
  }

  showGross(): void {
    this.totalCostRef.showGross();
  }

  onShownGross(grossCost: number): void {
    this.grossCost = grossCost;
  }

  canDeactivate() {
    if (!this.carForm.dirty) {
      return true;
    }

    return window.confirm('Discard changes?');
  }
}
