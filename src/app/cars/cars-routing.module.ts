import { NgModule } from "@angular/core";
import { RouterModule, Route } from '@angular/router';
import { CarsComponent } from './cars.component';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarDetailsComponent } from "./car-details/car-details.component";
import { CarResolve } from './car-resolve.service';
import { FormCanDeactivateGuard } from "../guards/form-can-deactivate.guard";

const carsRoutes: Route[] = [
  {
    path: '',
    component: CarsComponent,
    children: [
      { path: '', component: CarsListComponent, canDeactivate: [FormCanDeactivateGuard] },
      { path: ':id', component: CarDetailsComponent, resolve: { car: CarResolve } }]
  }
];

@NgModule({
  imports: [RouterModule.forChild(carsRoutes)],
  exports: [RouterModule]
})

export class CarsRoutingModule { }
