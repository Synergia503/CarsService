import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Car } from './models/car';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CarsService {

  private apiUrl: string = "http://localhost:3000/api/cars";

  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    return this.http.get<Car[]>(this.apiUrl)
      .map(res => res);
  }

  getCar(id: number): Observable<Car> {
    return this.http.get<Car>(this.apiUrl + `/${id}`)
      .map(res => res);
  }

  updateCar(id: number, data): Observable<Car> {
    return this.http.put<Car>(this.apiUrl + `/${id}`, data)
      .map(res => res);
  }

  addCar(data): Observable<Car> {
    return this.http.post<Car>(this.apiUrl, data)
      .map(res => res);
  }

  removeCar(id: number): Observable<Car> {
    return this.http.delete<Car>(this.apiUrl + `/${id}`)
      .map(res => res);
  }
}