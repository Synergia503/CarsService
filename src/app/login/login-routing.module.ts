import { NgModule } from "@angular/core";
import { RouterModule, Route } from '@angular/router';
import { LoginComponent } from "./login.component";

const loginRoutes: Route[] = [
    { path: 'login', component: LoginComponent }
];

@NgModule({
    imports: [RouterModule.forChild(loginRoutes)],
    exports: [RouterModule]
})

export class LoginRoutingModule { }